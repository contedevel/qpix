package com.contedevel.qpix.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.contedevel.qpix.R;
import com.contedevel.qpix.activity.MainActivity;
import com.contedevel.qpix.util.BitmapUtils;
import com.contedevel.qpix.util.Cache;
import com.contedevel.qpix.util.InstagramAPI;
import com.contedevel.qpix.util.NetStateReceiver;
import com.contedevel.qpix.util.adapter.ImageAdapter;
import com.contedevel.qpix.util.json.Media;
import com.contedevel.qpix.util.json.User;
import com.contedevel.qpix.util.view.ParallaxGallery;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by ConteDevel on 23.08.2016.
 */
public class MainFragment extends Fragment implements SearchView.OnSuggestionListener,
        SwipeRefreshLayout.OnRefreshListener, ImageAdapter.OnItemClickListener,
        ImageAdapter.OnLoadMoreListener, NetStateReceiver.OnNetStateChangedListener {

    private static final String TAG = MainFragment.class.getSimpleName();

    private static final int SEARCH_LOADER_ID   = 0;
    private static final int MEDIA_LOADER_ID    = 1;
    private static final int IMAGE_CACHE_LOADER = 3;

    private static final int IMAGES_PER_LOADING = 8;

    private MainActivity mActivity;

    private SearchView mSearchView;
    private SwipeRefreshLayout mSRLUpdater;
    private Snackbar mSnackbar;
    private ParallaxGallery mGallery;

    private SimpleCursorAdapter mAdapter;
    private ImageAdapter mImageAdapter;

    private Handler mHandler = new Handler();
    private String mSelectedId;
    private String mNextMediaMaxId;
    private String mRestoredQuery;

    private LoaderManager.LoaderCallbacks<User.UserPage> mUsersLoaderCallbacks =
            new LoaderManager.LoaderCallbacks<User.UserPage>() {
                @Override
                public Loader<User.UserPage> onCreateLoader(final int id, Bundle args) {

                    if (SEARCH_LOADER_ID == id) {
                        return new UsersLoader(getContext(), args.getString("query"), args.getInt("count"));
                    }

                    return null;
                }

                @Override
                public void onLoadFinished(Loader<User.UserPage> loader, User.UserPage data) {
                    Log.d(TAG, "onLoadFinished: " + String.valueOf(data.isOk()));
                    MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "user"});

                    if (data.isOk()) {

                        for (int i = 0; i < data.length(); ++i) {
                            User user = data.get(i);
                            c.addRow(new Object[]{user.getId(), user.getFullName()});
                        }
                    }

                    mAdapter.changeCursor(c);
                }

                @Override
                public void onLoaderReset(Loader<User.UserPage> loader) {
                }
            };

    private LoaderManager.LoaderCallbacks<Media.MediaPage> mMediaLoaderCallbacks =
            new LoaderManager.LoaderCallbacks<Media.MediaPage>() {
                @Override
                public Loader<Media.MediaPage> onCreateLoader(final int id, Bundle args) {

                    if (MEDIA_LOADER_ID == id) {
                        final int count = args.getInt("count");
                        final String minId = args.getString("min_id");
                        final String maxId = args.getString("max_id");
                        return new MediaLoader(getContext(), mSelectedId, count, maxId, minId);
                    }

                    return null;
                }

                @Override
                public void onLoadFinished(Loader<Media.MediaPage> loader, Media.MediaPage data) {
                    Log.d(TAG, "Data: " + data.toString());

                    if (data.isOk()) {
                        Media[] medias = data.getAll();
                        mNextMediaMaxId = data.getNextMaxId();

                        for (Media m : medias) {
                            mImageAdapter.add(m);
                        }

                        mImageAdapter.notifyDataSetChanged();
                        mImageAdapter.setLoaded();

                        mSRLUpdater.setRefreshing(false);
                        hideSnackbar();
                    }
                }

                @Override
                public void onLoaderReset(Loader<Media.MediaPage> loader) {
                    mSRLUpdater.setRefreshing(false);
                    hideSnackbar();
                }
            };

    private LoaderManager.LoaderCallbacks<List<Bitmap>> mBitmapsLoaderCallbacks =
            new LoaderManager.LoaderCallbacks<List<Bitmap>>() {

        @Override
        public Loader<List<Bitmap>> onCreateLoader(final int id, Bundle args) {

            if (IMAGE_CACHE_LOADER == id) {
                return new ImageCacheLoader(getContext(), 480, 480);
            }

            return null;
        }

        @Override
        public void onLoadFinished(Loader<List<Bitmap>> loader, List<Bitmap> data) {
            mGallery.addAll(data);
        }

        @Override
        public void onLoaderReset(Loader<List<Bitmap>> loader) {

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mActivity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initActionBar(view);
        final RecyclerView rvContent = (RecyclerView) view.findViewById(R.id.rvContent);
        RecyclerView.LayoutManager lm = new GridLayoutManager(getContext(), 2);
        rvContent.setHasFixedSize(true);
        rvContent.setLayoutManager(lm);
        mImageAdapter = new ImageAdapter(rvContent);
        mImageAdapter.addOnItemClickListener(this);
        mImageAdapter.setOnLoadMoreListener(this);
        rvContent.setAdapter(mImageAdapter);
        final AppBarLayout appBar = (AppBarLayout) view.findViewById(R.id.appBar);
        rvContent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            int primaryColor = ContextCompat.getColor(getContext(), R.color.primary);
            float elevation = getContext().getResources().getDimension(R.dimen.app_bar_elevation);
            boolean isTinted = false;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if(!rvContent.canScrollVertically(-1)) {
                    appBar.setBackgroundColor(0x00000000);
                    ViewCompat.setElevation(appBar, 0);
                    mSRLUpdater.setEnabled(true);
                    isTinted = false;
                } else if (!isTinted){
                    appBar.setBackgroundColor(primaryColor);
                    ViewCompat.setElevation(appBar, elevation);
                    mSRLUpdater.setEnabled(false);
                    isTinted = true;
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, final int newState) {
                mSRLUpdater.setEnabled(!rvContent.canScrollVertically(-1));
            }
        });
        mSRLUpdater = (SwipeRefreshLayout) view.findViewById(R.id.srlUpdater);
        mSRLUpdater.setOnRefreshListener(this);
        mSRLUpdater.setEnabled(false);
        //Load background images
        mGallery = (ParallaxGallery) view.findViewById(R.id.gallery);
        getLoaderManager()
                .initLoader(IMAGE_CACHE_LOADER, null, mBitmapsLoaderCallbacks)
                .forceLoad();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            mRestoredQuery = savedInstanceState.getString("query");
            mSelectedId = savedInstanceState.getString("user_id");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("query", mSearchView.getQuery().toString());
        outState.putString("user_id", mSelectedId);
        mNextMediaMaxId = null;
    }

    private void initActionBar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setTitleMarginStart(0);
        mActivity.setSupportActionBar(toolbar);
        ActionBar actionBar = mActivity.getSupportActionBar();
        DrawerLayout drawerLayout = mActivity.getDrawerLayout();
        //Synchronize with DrawerLayout
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(mActivity,
                drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        drawerToggle.syncState();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG, "onPrepareOptionsMenu");
        super.onPrepareOptionsMenu(menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);

        if(searchItem != null) {
            mSearchView = (SearchView) MenuItemCompat.getActionView(searchItem);
            SearchManager searchManager = (SearchManager) mActivity
                    .getSystemService(Activity.SEARCH_SERVICE);
            mSearchView.setSearchableInfo(searchManager
                    .getSearchableInfo(mActivity.getComponentName()));
            mSearchView.onActionViewExpanded();
            final String[] from = new String[]{"user"};
            final int[] to = new int[]{android.R.id.text1};
            mAdapter = new SimpleCursorAdapter(getActivity(),
                    android.R.layout.simple_list_item_1, null,
                    from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
            mSearchView.setSuggestionsAdapter(mAdapter);
            mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(final String s) {
                    return s.length() < 3;
                }

                @Override
                public boolean onQueryTextChange(final String s) {
                    mNextMediaMaxId = null;
                    mImageAdapter.clear();
                    mImageAdapter.notifyDataSetChanged();
                    populateAdapter(s);
                    return true;
                }
            });
            mSearchView.setOnSuggestionListener(this);

            if (!TextUtils.isEmpty(mRestoredQuery)) {
                mSearchView.setQuery(mRestoredQuery, true);
                restartMediaLoader(null, null);
            }

            mSearchView.post(new Runnable() {
                @Override
                public void run() {
                    mActivity.hideKeyboard();
                }
            });
        }
    }

    private void populateAdapter(final String query) {

        if(!TextUtils.isEmpty(mRestoredQuery)) {
            mRestoredQuery = null;
            return;
        }

        if(query.length() < 3) {
            MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "user"});
            mAdapter.changeCursor(c);
            return;
        }

        mHandler.removeCallbacksAndMessages(null);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Bundle args = new Bundle(2);
                args.putString("query", query);
                args.putInt("count", 3);
                getLoaderManager()
                        .restartLoader(SEARCH_LOADER_ID, args, mUsersLoaderCallbacks)
                        .forceLoad();
            }
        }, 300);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (R.id.home == item.getItemId()) {
            mActivity.getDrawerLayout().openDrawer(GravityCompat.START);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSuggestionSelect(final int position) {
        return true;
    }

    @Override
    public boolean onSuggestionClick(final int position) {
        MatrixCursor cursor = (MatrixCursor)mAdapter.getCursor();
        mSelectedId = cursor.getString(0);
        final String query = cursor.getString(1);
        mSearchView.setQuery(query, true);
        restartMediaLoader(null, null);
        return true;
    }

    @Override
    public void onRefresh() {

        if(mSelectedId != null) {
            mImageAdapter.clear();
            mImageAdapter.setLoaded();
            hideSnackbar();
            mNextMediaMaxId = null;
            restartMediaLoader(null, null);
        } else {
            mSRLUpdater.setRefreshing(false);
        }
    }

    private void hideSnackbar() {

        if(mSnackbar != null) {
            mSnackbar.dismiss();
            mSnackbar = null;
        }
    }

    private void restartMediaLoader(@Nullable final String minId, @Nullable final String maxId) {
        Bundle args = new Bundle(3);
        args.putInt("count", IMAGES_PER_LOADING);

        args.putString("min_id", minId);
        args.putString("max_id", maxId);
        getLoaderManager()
                .restartLoader(MEDIA_LOADER_ID, args, mMediaLoaderCallbacks)
                .forceLoad();
    }

    @Override
    public void onItemClick(final int position, Media media) {
        FragmentManager fm = mActivity.getSupportFragmentManager();
        Fragment fragment = new DetailsFragment();
        Bundle args = new Bundle(1);
        args.putSerializable("media", media);
        fragment.setArguments(args);
        fm.beginTransaction()
                .add(R.id.container, fragment, "DetailsFragment")
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onLoadMore() {

        if(TextUtils.isEmpty(mNextMediaMaxId)) {
            return;
        }

        restartMediaLoader(null, mNextMediaMaxId);

        View view = getView();

        if(view != null) {
            mSnackbar = Snackbar.make(view, R.string.msg_loading, Snackbar.LENGTH_INDEFINITE);
            mSnackbar.show();
        }
    }

    @Override
    public void onNetStateChanged(final boolean hasConnection) {
        //Just it's a stub. To make the real Internet access listener
        //it needs to save states of all running processes (e.g. in Bundle)
        // and restart|continue it when the Internet access will appear again.

        if (!hasConnection) {
            hideSnackbar();

            View view = getView();

            if (view != null) {
                mSnackbar = Snackbar.make(view, R.string.error_no_connection,
                        Snackbar.LENGTH_INDEFINITE);
                mSnackbar.show();
            }
        } else {
            hideSnackbar();
        }
    }

    public static class UsersLoader extends AsyncTaskLoader<User.UserPage> {
        private InstagramAPI mApi;
        private final String mQuery;
        private final int mMaxCount;

        public UsersLoader(Context context, final String query, final int maxCount) {
            super(context);
            mApi = new InstagramAPI(context);
            mQuery = query;
            mMaxCount = maxCount;
        }

        @Override
        public User.UserPage loadInBackground() {

            try {
                return mApi.findUsers(mQuery, mMaxCount);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new User.UserPage();
        }
    }

    public static class MediaLoader extends AsyncTaskLoader<Media.MediaPage> {

        private InstagramAPI mApi;
        private String mUserId;
        private String mMinId;
        private String mMaxId;
        private int mCount;

        public MediaLoader(Context context, final String userId, final int count,
                           final String maxId, final String minId) {
            super(context);
            mApi = new InstagramAPI(getContext());
            mUserId = userId;
            mCount = count;
            mMaxId = maxId;
            mMinId = minId;
        }

        @Override
        public Media.MediaPage loadInBackground() {

            try {
                return mApi.findMedia(mUserId, mCount, mMinId, mMaxId);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new Media.MediaPage();
        }
    }

    public static class ImageCacheLoader extends AsyncTaskLoader<List<Bitmap>> {

        private File[] mFiles;
        private int mWidth;
        private int mHeight;

        public ImageCacheLoader(final Context context, final int width, final int height) {
            super(context);
            mWidth = width;
            mHeight = height;
            File imgs = new File(context.getCacheDir(), Cache.DIR_IMG);

            if (imgs.exists()) {
                final Pattern p = Pattern.compile(".+\\.1");
                mFiles = imgs.listFiles(new FileFilter() {
                    int count = 9;

                    @Override
                    public boolean accept(File file) {

                        if (file.isFile() && p.matcher(file.getName()).matches()) {
                            --count;
                            return count >= 0;
                        }

                        return false;
                    }
                });
            }
        }

        @Override
        public List<Bitmap> loadInBackground() {
            List<Bitmap> bitmaps = new ArrayList<>();

            if(mFiles == null) {
                return bitmaps;
            }

            for(File f : mFiles) {

                if (f.exists()) {
                    Bitmap bmp = BitmapUtils
                            .decodeSampledBitmap(f.getAbsolutePath(), mWidth, mHeight);
                    bitmaps.add(bmp);
                }
            }

            return bitmaps;
        }
    }
}
