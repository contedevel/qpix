package com.contedevel.qpix.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.contedevel.qpix.R;
import com.contedevel.qpix.activity.MainActivity;
import com.contedevel.qpix.util.json.Media;
import com.squareup.picasso.Picasso;

/**
 * Created by ConteDevel on 29.08.2016.
 */
public class DetailsFragment extends Fragment {

    private MainActivity mActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        mActivity = (MainActivity) getActivity();

        Bundle args = getArguments();

        if(args != null && args.containsKey("media")) {
            ImageView image = (ImageView) view.findViewById(R.id.image);
            Media media = (Media) args.getSerializable("media");
            String title = null;

            if(media != null) {
                Picasso.with(getContext())
                        .load(media.getOriginSizeUrl())
                        .into(image);
                title = media.getId();
            }

            initActionBar(view, title);
        } else {
            initActionBar(view, null);
        }

        mActivity.hideKeyboard();
        return view;
    }

    private void initActionBar(View view, @Nullable final String title) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setTitleMarginStart(0);

        if(!TextUtils.isEmpty(title)) {
            toolbar.setTitle(title);
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActivity.onBackPressed();
            }
        });
    }
}
