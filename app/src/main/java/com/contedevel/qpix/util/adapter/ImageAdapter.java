package com.contedevel.qpix.util.adapter;

import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.contedevel.qpix.R;
import com.contedevel.qpix.util.ColorUtils;
import com.contedevel.qpix.util.json.Media;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ConteDevel on 24.08.2016.
 */
public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private static final String TAG = ImageAdapter.class.getSimpleName();

    private static final int VISIBLE_THRESHOLD = 5;

    private List<Media> mRecords = new ArrayList<>();
    private List<OnItemClickListener> mListeners = new ArrayList<>();
    private OnLoadMoreListener mOnLoadMoreListener;
    private Picasso mPicasso;
    private int mMaxSize = 480;

    private boolean mIsLoading;

    public ImageAdapter(RecyclerView rv) {
        mPicasso = Picasso.with(rv.getContext());
        final GridLayoutManager lm = (GridLayoutManager) rv.getLayoutManager();
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, final int dx, final int dy) {
                super.onScrolled(recyclerView, dx, dy);

                final int totalItemCount = lm.getItemCount();
                final int lastVisibleItem = lm.findLastVisibleItemPosition();

                if (mOnLoadMoreListener != null && !mIsLoading
                        && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    mOnLoadMoreListener.onLoadMore();
                    mIsLoading = true;
                }
            }
        });
    }

    public void add(final Media record) {
        mRecords.add(record);
    }

    public Media get(final int position) {
        return mRecords.get(position);
    }

    public void clear() {
        mRecords.clear();
    }

    public void addOnItemClickListener(@Nullable OnItemClickListener listener) {
        mListeners.add(listener);
    }

    public boolean removeOnItemClickListener(@Nullable OnItemClickListener listener) {
        return mListeners.remove(listener);
    }

    public void setOnLoadMoreListener(@Nullable OnLoadMoreListener onLoadMoreListener) {
        mOnLoadMoreListener = onLoadMoreListener;
    }

    public void setMaxSize(final int maxSize) {
        mMaxSize = maxSize;
    }

    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.li_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageAdapter.ViewHolder holder, final int position) {
        holder.mImage.setBackgroundColor(ColorUtils.generateColor(0.5f));
        mPicasso.load(mRecords.get(position).getSmallSizeUrl())
                .resize(mMaxSize, mMaxSize)
                .centerCrop()
                .into(holder.mImage);
    }

    @Override
    public int getItemCount() {
        return mRecords.size();
    }

    public void setLoaded() {
        mIsLoading = false;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImage;

        public ViewHolder(View itemView) {
            super(itemView);
            mImage = (ImageView) itemView;
            mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (OnItemClickListener listener : mListeners) {

                        if (listener != null) {
                            final int position = getAdapterPosition();
                            listener.onItemClick(position, mRecords.get(position));
                        } else {
                            //Remove all nulls
                            mListeners.removeAll(Collections.singleton(listener));
                        }
                    }
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(final int position, Media media);
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }
}
