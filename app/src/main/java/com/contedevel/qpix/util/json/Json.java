package com.contedevel.qpix.util.json;

import com.contedevel.qpix.util.ILazyInstance;

import java.io.Serializable;

/**
 * Created by ConteDevel on 23.08.2016.
 */
public abstract class Json implements ILazyInstance, Serializable {

    private static final long serialVersionUID = -75272266424254522L;

    protected boolean mIsOk = false;
    /**
     * Sets {@see java.lang.String String} array to build
     * {@see org.json.JSONObject JSONObject} array. You
     * must call this method before instance using.
     * @param args JSON-strings or some else parameters
     */
    public abstract void init(String... args);

    /**
     * Indicates whether the instance is okay and you can
     * use it.
     * @return True if it's OK, otherwise - false
     */
    public boolean isOk() {
        return mIsOk;
    }

    @Override
    public String toString() {
        return "JSON {\n \tis_ok: " + String.valueOf(mIsOk) + ";\n }";
    }

    public static boolean isEmpty(final String str) {
        return str == null || str.equals("") || str.equalsIgnoreCase("null");
    };
}
