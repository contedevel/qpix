package com.contedevel.qpix.util;

import android.support.annotation.Nullable;

import java.util.Iterator;

/**
 * Created by ConteDevel on 22.08.2016.
 */
public final class Values implements Iterable<Values.Value> {
    private static final int INITIAL_CAPACITY = 4;
    private String[] mKeys;
    private String[] mValues;
    private int mLength = 0;

    public Values(final int capacity) {
        init(capacity > 0 ? capacity : INITIAL_CAPACITY);
    }

    public Values() {
        init(INITIAL_CAPACITY);
    }

    private void init(int capacity) {
        mKeys = new String[capacity];
        mValues = new String[capacity];
    }

    public void increase(final int additionalLength) {
        String[] tmpKeys = new String[mKeys.length + additionalLength];
        String[] tmpValues = new String[mValues.length + additionalLength];
        System.arraycopy(mKeys, 0, tmpKeys, 0, mLength);
        System.arraycopy(mValues, 0, tmpValues, 0, mLength);
        mKeys = tmpKeys;
        mValues = tmpValues;
    }

    public void add(final String key, final String value) {

        if(mLength < mKeys.length) {
            increase(1);
        }

        mKeys[mLength] = ObjectsCompat.requireNotNull(key, "Key cannot be null!");
        mValues[mLength++] = ObjectsCompat.requireNotNull(value, "Value cannot be null!");
    }

    @Nullable
    public final String get(final String key) {

        for(int i = 0; i < mLength; ++i) {

            if(key.equals(mKeys[i])) {
                return mValues[i];
            }
        }

        return null;
    }

    public final String getKey(final int position) {
        return mKeys[position];
    }

    public final String getValue(final int position) {
        return mValues[position];
    }

    public String[] getKeys() {
        String[] keys = new String[mLength];
        System.arraycopy(mKeys, 0, keys, 0, mLength);
        return keys;
    }

    public String[] getValues() {
        String[] values = new String[mLength];
        System.arraycopy(mValues, 0, values, 0, mLength);
        return values;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("{ ");

        if(mLength > 0) {
            final int loops = mLength - 1;

            for (int i = 0; i < loops; ++i) {
                result.append("\"").append(mKeys[i]).append("\":\"").append(mValues[i]).append("\",");
            }

            result.append("\"").append(mKeys[loops]).append("\":\"")
                    .append(mValues[loops]).append("\" }");
        } else {
            return "{none}";
        }

        return result.toString();
    }

    @Override
    public Iterator<Value> iterator() {
        return new ValueIterator();
    }

    public int size() {
        return mLength;
    }

    public static final class Value {
        public String key;
        public String value;

        public Value(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    public class ValueIterator implements Iterator<Value> {
        private int mIndex = 0;

        @Override
        public boolean hasNext() {
            return mIndex < mLength;
        }

        @Override
        public Value next() {
            return new Value(mKeys[mIndex], mValues[mIndex++]);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Removal of　the object is not allowed!");
        }
    }
}
