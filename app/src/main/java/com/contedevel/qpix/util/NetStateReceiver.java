package com.contedevel.qpix.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by ConteDevel on 29.08.2016.
 */
public abstract class NetStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        onNetStateChanged(hasInternetAccess(context));
    }

    /**
     * Calls on the Internet access state change
     * @param hasConnection True if device has the Internet access, otherwise - false
     */
    public abstract void onNetStateChanged(boolean hasConnection);

    /**
     * Checks whether device has some available net
     * @param context Application context
     * @return Check result
     */
    public static boolean hasInternetAccess(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }

    /**
     * This interface helps to listen changes of the Internet access state
     */
    public interface OnNetStateChangedListener {

        /**
         * Calls on the Internet access state change
         * @param hasConnection True if device has the Internet access, otherwise - false
         */
        void onNetStateChanged(boolean hasConnection);
    }
}
