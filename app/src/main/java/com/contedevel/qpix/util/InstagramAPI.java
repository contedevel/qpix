package com.contedevel.qpix.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.contedevel.qpix.util.json.Media;
import com.contedevel.qpix.util.json.User;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ConteDevel on 22.08.2016.
 */
public class InstagramAPI {

    private static final String TAG = InstagramAPI.class.getSimpleName();

    private static final String SECRET = "201819d7a8574a0baee3337323fce181";

    public static final String ACCOUNT_TYPE = "qpix.instagram";
    public static final String CLIENT_ID = "d919439fe69844129703425fdf8a1ff6";
    public static final String REDIRECT_URI = "https://com.contedevel.qpix/instagram/auth";

    public static final String URL_DOMAIN = "https://api.instagram.com";
    public static final String URL_AUTH = "/oauth/authorize/";
    public static final String URL_ACCESS_TOKEN = "/oauth/access_token";
    public static final String URL_USER_SEARCH = "/v1/users/search";

    public static final String KEY_ACCESS_TOKEN = "access_token";
    public static final String KEY_AVATAR_URL = "avatar_url";
    public static final String KEY_CLIENT_ID = "client_id";
    public static final String KEY_CLIENT_SECRET = "client_secret";
    public static final String KEY_CODE = "code";
    public static final String KEY_FULL_NAME = "full_name";
    public static final String KEY_ID = "id";
    public static final String KEY_REDIRECT_URI = "redirect_uri";
    public static final String KEY_RESPONSE_TYPE = "response_type";

    private AccountManager mAccountManager;
    private Account mAccount;
    private String mAccessToken;
    private Pattern mInvalidTokenPattern = Pattern.compile(".*ERROR_TYPE.*OAuthAccessTokenError.*");

    public InstagramAPI(Context context) {
        mAccountManager = AccountManager.get(context);
        mAccount = ObjectsCompat.requireNotNull(getAccount(), "Account is null!");
        mAccessToken = mAccountManager.getUserData(mAccount, KEY_ACCESS_TOKEN);
    }

    @Nullable
    public static String requestToken(final String code) throws IOException {
        Values vs = new Values(5);
        vs.add(KEY_CLIENT_ID, CLIENT_ID);
        vs.add(KEY_CLIENT_SECRET, SECRET);
        vs.add("grant_type", "authorization_code");
        vs.add(KEY_REDIRECT_URI, REDIRECT_URI);
        vs.add(KEY_CODE, code);
        HttpsURLConnection connection = new HttpsRequest(URL_DOMAIN + URL_ACCESS_TOKEN)
                .setRequestMethod(HttpsRequest.POST)
                .setBodyArguments(vs)
                .setCacheMode(CacheMode.DISABLED)
                .build();

        String answer = null;
        ;

        if (HttpsURLConnection.HTTP_OK == connection.getResponseCode()) {
            answer = ConnectionUtils.readToString(connection.getInputStream());
        }

        connection.disconnect();
        return answer;
    }

    public User.UserPage findUsers(final String query, final int maxCount) throws IOException {
        StringBuilder url = new StringBuilder(URL_DOMAIN + URL_USER_SEARCH + "?")
                .append("q=").append(URLEncoder.encode(query, "UTF-8"))
                .append("&").append(KEY_ACCESS_TOKEN).append("=").append(mAccessToken);

        if (maxCount > 0) {
            url.append("&").append("count").append(maxCount);
        }

        HttpsURLConnection connection = new HttpsRequest(url.toString())
                .setCacheMode(CacheMode.DISABLED)
                .build();
        final String answer = ConnectionUtils.readToString(connection.getInputStream());

        connection.disconnect();

        User.UserPage page = new User.UserPage();

        if (!TextUtils.isEmpty(answer)) {

            if(!isValidToken(answer)) {
                refreshToken();
                findUsers(query, maxCount);
            }

            page.init(answer);
        }

        return page;
    }

    public Media.MediaPage findMedia(final String userId, final int count,
                                     final String minId, final String maxId) throws IOException {
        StringBuilder url = new StringBuilder(URL_DOMAIN + "/v1/users/")
                .append(userId).append("/media/recent/?")
                .append(KEY_ACCESS_TOKEN).append("=").append(mAccessToken);

        if(count > 0) {
            url.append("&count=");
            url.append(count);
        }

        if(!TextUtils.isEmpty(minId)) {
            url.append("&min_id=");
            url.append(minId);
        }

        if(!TextUtils.isEmpty(maxId)) {
            url.append("&max_id=");
            url.append(maxId);
        }

        HttpsURLConnection connection = new HttpsRequest(url.toString())
                .setCacheMode(CacheMode.ENABLED)
                .build();
        final String answer = ConnectionUtils.readToString(connection.getInputStream());
        Log.d(TAG, "Media: " + answer);

        connection.disconnect();

        Media.MediaPage page = new Media.MediaPage();

        if (!TextUtils.isEmpty(answer)) {

            if(!isValidToken(answer)) {
                refreshToken();
                findMedia(userId, count, minId, maxId);
            }

            page.init(answer);
        }

        return page;
    }

    private void refreshToken() {
        mAccountManager.invalidateAuthToken(ACCOUNT_TYPE, KEY_ACCESS_TOKEN);
        AccountManagerFuture<Bundle> authToken = mAccountManager.getAuthToken(mAccount,
                KEY_ACCESS_TOKEN, null, false, null, null);
        try {
            Bundle result = authToken.getResult();
            mAccessToken = result.getString(AccountManager.KEY_AUTHTOKEN);
        } catch (OperationCanceledException | IOException | AuthenticatorException e) {
            e.printStackTrace();
        }

        mAccount = ObjectsCompat.requireNotNull(getAccount(), "Account is null!");
    }

    @Nullable
    private Account getAccount() {
        Account[] accounts = mAccountManager.getAccountsByType(ACCOUNT_TYPE);

        if (accounts.length > 0) {
            return accounts[0];
        }

        return null;
    }

    public boolean isValidToken(final String answer) {
        Matcher m = mInvalidTokenPattern.matcher(answer);
        return !m.matches();
    }
}
