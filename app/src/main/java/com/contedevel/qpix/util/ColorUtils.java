package com.contedevel.qpix.util;

/**
 * Created by ConteDevel on 25.08.2016.
 */
public final class ColorUtils {
    //private static final String TAG = ColorUtils.class.getSimpleName();

    private ColorUtils() {
    }

    /**
     * Generates color with value 0.5 for lightness in HSL color model
     * @return ARGB color in 0xAARRGGBB format
     */
    public static int generateColor(final float light) {
        float hue = (float)Math.random();
        float saturation = (float)Math.sqrt(Math.random());
        return colorFromHSL(hue, saturation, light);
    }

    /**
     * Builds ARGB-color from HSL-color
     * @param h Hue
     * @param s Saturation
     * @param l Lightness
     * @return ARGB-color in 0xAARRGGBB format (not transparent)
     */
    public static int colorFromHSL(final float h, final float s, final float l) {
        float[] t = new float[3];
        int[] chs = new int[3];
        final float f = 1.0f / 3.0f;
        final float q = l < 0.5f ? l * (1.0f + s) : l + s - l * s;
        final float p = 2.0f * l - q;
        t[0] = h + f;
        t[1] = h;
        t[2] = h - f;

        for(int i = 0; i < 3; ++i) {
            float tC = t[i];

            if(tC < 0.0) {
                ++tC;
            } else if(tC > 1.0) {
                --tC;
            }

            float value;

            if(tC < (1.0f / 6.0f)) {
                value = p + ((q - p) * 6.0f * tC);
            } else if(tC < 0.5f) {
                value = q;
            } else if(tC < (2.0f / 3.0f)) {
                value = p + ((q - p) * 6.0f * (2.0f / 3.0f - tC));
            } else {
                value = p;
            }

            chs[i] = Math.round(value * 255);
        }

        return ((((0xff00 + chs[0]) << 8) + chs[1]) << 8) + chs[2];
    }
}