package com.contedevel.qpix.util.json;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ConteDevel on 23.08.2016.
 */
public abstract class Page<T extends Json> extends Json {

    private static final long serialVersionUID = 1770066590504805258L;

    private static final String TAG = Page.class.getSimpleName();

    protected String mNextUrl;
    protected String mNextMaxId;
    protected T[] mJsons;

    public String getNextUrl() {
        return mNextUrl;
    }

    public boolean hasNext() {
        return !TextUtils.isEmpty(mNextUrl);
    }

    public String getNextMaxId() {
        return mNextMaxId;
    }

    public boolean hasNextMaxId() {
        return !TextUtils.isEmpty(mNextMaxId);
    }

    public T get(final int index) {
        return mJsons[index];
    }

    public T[] getAll() {
        return mJsons;
    }

    public int length() {
        return mJsons.length;
    }

    @Override
    public void init(String... args) {
        final String jsonStr = args[0];

        try {
            JSONObject json = new JSONObject(jsonStr);

            if (json.has("pagination")) {
                JSONObject jsonPagination = json.getJSONObject("pagination");

                if(jsonPagination.has("nex_url")) {
                    mNextUrl = jsonPagination.getString("next_url");
                }

                if(jsonPagination.has("next_max_id")) {
                    mNextMaxId = jsonPagination.getString("next_max_id");
                }
            }

            JSONArray jsonData = json.getJSONArray("data");
            mJsons = onArrayCreation(jsonData.length());
            boolean state = true;

            for(int i = 0; i < jsonData.length(); ++i) {
                String userStr = jsonData.getString(i);
                mJsons[i] = onChildCreation();
                mJsons[i].init(userStr);

                if(!mJsons[i].isOk()) {
                    state = false;
                }
            }

            mIsOk = state;
        } catch (JSONException e) {
            e.printStackTrace();
            mIsOk = false;
        }
    }

    public abstract T[] onArrayCreation(final int size);

    public abstract T onChildCreation();

    @Override
    public String toString() {
        return "JSON page {\n "
                + "\tis_ok: " + String.valueOf(mIsOk) + ";\n "
                + "\tnext_url: " + mNextUrl + ";\n "
                + "\tJSONs: " + String.valueOf(mJsons == null ? 0 : mJsons.length) + ";\n }";
    }
}
