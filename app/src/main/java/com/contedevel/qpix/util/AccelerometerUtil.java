package com.contedevel.qpix.util;

/**
 * Created by ConteDevel on 29.08.2016.
 */
public abstract class AccelerometerUtil {

    private AccelerometerUtil() {}

    public static void canonicalToScreenOrientation(final int displayRotation,
                                                    final float[] canVec,
                                                    final float[] screenVec) {
        final int axisSwap[][] = {
                { 1,  -1,  0,  1  },    // ROTATION_0
                {-1,  -1,  1,  0  },    // ROTATION_90
                {-1,   1,  0,  1  },    // ROTATION_180
                { 1,   1,  1,  0  }     // ROTATION_270
        };
        final int[] as = axisSwap[displayRotation];
        screenVec[0]  =  (float)as[0] * canVec[ as[2] ];
        screenVec[1]  =  (float)as[1] * canVec[ as[3] ];
        screenVec[2]  =  canVec[2];
    }
}
