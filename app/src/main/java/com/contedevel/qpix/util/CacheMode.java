package com.contedevel.qpix.util;

/**
 * Created by ConteDevel on 22.08.2016.
 */
public enum CacheMode {
    ENABLED,
    OFFLINE,
    UPDATE,
    DISABLED
}
