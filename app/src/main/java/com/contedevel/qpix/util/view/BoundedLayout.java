package com.contedevel.qpix.util.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.contedevel.qpix.R;

/**
 * Created by ConteDevel on 22.08.2016.
 */
public class BoundedLayout extends LinearLayout {

    private final int mBoundedWidth;

    private final int mBoundedHeight;

    public BoundedLayout(Context context) {
        super(context);
        mBoundedWidth = 0;
        mBoundedHeight = 0;
    }

    public BoundedLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.BoundedLayout);
        mBoundedWidth = a.getDimensionPixelSize(R.styleable.BoundedLayout_boundedWidth, 0);
        mBoundedHeight = a.getDimensionPixelSize(R.styleable.BoundedLayout_boundedHeight, 0);
        a.recycle();
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        // Adjust width as necessary
        final int measuredWidth = MeasureSpec.getSize(widthMeasureSpec);
        int newWidth = widthMeasureSpec;

        if (mBoundedWidth > 0 && mBoundedWidth < measuredWidth) {
            int measureMode = MeasureSpec.getMode(widthMeasureSpec);
            newWidth = MeasureSpec.makeMeasureSpec(mBoundedWidth, measureMode);
        }

        // Adjust height as necessary
        final int measuredHeight = MeasureSpec.getSize(heightMeasureSpec);
        int newHeight = heightMeasureSpec;

        if (mBoundedHeight > 0 && mBoundedHeight < measuredHeight) {
            int measureMode = MeasureSpec.getMode(heightMeasureSpec);
            newHeight = MeasureSpec.makeMeasureSpec(mBoundedHeight, measureMode);
        }

        super.onMeasure(newWidth, newHeight);
    }
}
