package com.contedevel.qpix.util.json;

import android.app.Notification;
import android.graphics.Point;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ConteDevel on 25.08.2016.
 */
public class Media extends Json {

    private static final long serialVersionUID = -1844410791758873429L;

    private String mId;
    private String mSmallSizeUrl;
    private String mOriginSizeUrl;
    private Point mSmallSize;
    private Point mOriginSize;
    private String mType;
    private String mLink;

    public String getId() {
        return mId;
    }

    public Point getOriginSize() {
        return mOriginSize;
    }

    public String getOriginSizeUrl() {
        return mOriginSizeUrl;
    }

    public Point getSmallSize() {
        return mSmallSize;
    }

    public String getSmallSizeUrl() {
        return mSmallSizeUrl;
    }

    public String getType() {
        return mType;
    }

    public String getLink() {
        return mLink;
    }

    @Override
    public void init(String... args) {
        final String jsonStr = args[0];

        try {
            JSONObject json = new JSONObject(jsonStr);
            mId = json.getString("id");
            mType = json.getString("type");
            mLink = json.getString("link");
            JSONObject jsonImages = json.getJSONObject("images");

            if(jsonImages != null) {
                JSONObject jsonSmallSize = jsonImages.getJSONObject("low_resolution");
                mSmallSizeUrl = jsonSmallSize.getString("url");
                mSmallSize = new Point(jsonSmallSize.getInt("width"),
                        jsonSmallSize.getInt("height"));
                JSONObject jsonOriginSize = jsonImages.getJSONObject("standard_resolution");
                mOriginSizeUrl = jsonOriginSize.getString("url");
                mOriginSize = new Point(jsonOriginSize.getInt("width"),
                        jsonOriginSize.getInt("height"));

                mIsOk = true;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            mIsOk = false;
        }
    }

    public static class MediaPage extends Page<Media> {

        private static final long serialVersionUID = 6536645737613213910L;

        @Override
        public Media[] onArrayCreation(final int size) {
            return new Media[size];
        }

        @Override
        public Media onChildCreation() {
            return new Media();
        }
    }
}
