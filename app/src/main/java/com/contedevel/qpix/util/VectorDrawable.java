package com.contedevel.qpix.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.graphics.drawable.VectorDrawableCompat;

/**
 * Created by ConteDevel on 25.08.2016.
 */
public final class VectorDrawable {

    private VectorDrawable() {}

    public static Drawable getDrawable(Context context, @DrawableRes final int resId) {
        return VectorDrawableCompat.create(context.getResources(), resId, context.getTheme());
    }
}
