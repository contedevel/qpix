package com.contedevel.qpix.util;

import android.text.TextUtils;

/**
 * Created by ConteDevel on 22.08.2016.
 */
public abstract class ObjectsCompat {

    private ObjectsCompat() {}

    public static <T> T requireNotNull(T obj, final String msg) {

        if(obj == null) {
            throw new NullPointerException(msg);
        }

        return obj;
    }

    public static String requireNotEmpty(String obj, final String msg) {

        if(TextUtils.isEmpty(msg)) {
            throw new NullPointerException(msg);
        }

        return obj;
    }
}
