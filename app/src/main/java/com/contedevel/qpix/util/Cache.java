package com.contedevel.qpix.util;

import android.content.Context;
import android.net.http.HttpResponseCache;
import android.util.Log;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

/**
 * Created by ConteDevel on 26.08.2016.
 */
public abstract class Cache {

    private static final String TAG = Cache.class.getSimpleName();

    public static final String DIR_IMG = "img";
    public static final String DIR_HTTP = "http";

    private static final int CACHE_SIZE_IMG = 15 * 1024 * 1024;
    private static final int CACHE_SIZE_HTTP = 10 * 1024 * 1024;

    private static Picasso sPicassoInstance;

    private Cache() {
    } //To prevent an instances creation

    public static boolean enable(Context context) {

        if (sPicassoInstance == null) {
            File imgCacheDir = new File(context.getCacheDir(), DIR_IMG);
            OkHttpDownloader downloader = new OkHttpDownloader(imgCacheDir, CACHE_SIZE_IMG);
            sPicassoInstance = new Picasso.Builder(context)
                    .downloader(downloader)
                    .build();
            Picasso.setSingletonInstance(sPicassoInstance);
        }

        try {
            File httpCacheDir = new File(context.getCacheDir(), DIR_HTTP);
            HttpResponseCache.install(httpCacheDir, CACHE_SIZE_HTTP);
            return true;
        } catch (IOException e) {
            Log.i(TAG, "HTTP response cache installation failed:" + e);
        }

        return false;
    }

    public static void disable() {
        HttpResponseCache cache = HttpResponseCache.getInstalled();

        if (cache != null) {
            cache.flush();
        }
    }
}
