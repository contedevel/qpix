package com.contedevel.qpix.util;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by ConteDevel on 22.08.2016.
 */
public class HttpsRequest {

    private static final String TAG = HttpsRequest.class.getSimpleName();

    public static final int CONNECTION_TIMEOUT = 4000;

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String DELETE = "DELETE";

    /**
     * The property name for HTTP-request to control caching
     */
    public static final String HTTP_CACHE_PROPERTY = "Cache-Control";

    /**
     * Uses to prevent get data from the HTTP-cache
     */
    public static final String NO_HTTP_CACHE = "no-cache";

    /**
     * Uses to get data only from the HTTP-cache
     */
    public static final String ONLY_HTTP_CACHE = "only-if-cached";

    /**
     * Uses to force load a data from net and update the HTTP-cache
     */
    public static final String UPDATE_HTTP_CACHE = "max-age=0";

    private String mPath;
    private String mRequestMethod = GET;
    private Values mArgs;
    private Values mBodyArgs;
    private int mTimeout = CONNECTION_TIMEOUT;
    private String mCacheMode;
    private IPostRequestConfig mRequestConfig;

    public HttpsRequest(final String path) {
        mPath = ObjectsCompat.requireNotEmpty(path, "URL cannot be null");
    }

    public HttpsRequest setRequestMethod(final String requestMethod) {
        mRequestMethod = requestMethod;
        return this;
    }

    public HttpsRequest setPostRequestConfig(IPostRequestConfig config) {
        mRequestMethod = POST;
        mRequestConfig = config;
        return this;
    }

    public HttpsRequest setConnectionTimeout(final int timeout) {
        mTimeout = timeout;
        return this;
    }

    public HttpsRequest setRequestArguments(@Nullable Values args) {
        mArgs = args;
        return this;
    }

    public HttpsRequest setBodyArguments(@Nullable Values args) {
        mBodyArgs = args;
        return this;
    }

    public HttpsRequest setCacheMode(CacheMode cacheMode) {

        switch (cacheMode) {
            case UPDATE:
                mCacheMode = HttpsRequest.UPDATE_HTTP_CACHE;
                return this;
            case OFFLINE:
                mCacheMode = HttpsRequest.ONLY_HTTP_CACHE;
                return this;
            case DISABLED:
                mCacheMode = HttpsRequest.NO_HTTP_CACHE;
        }

        return this;
    }

    public HttpsURLConnection build() throws IOException {
        URL url = new URL(mPath);
        HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
        connection.setRequestMethod(mRequestMethod);
        connection.setConnectTimeout(mTimeout);

        if(!TextUtils.isEmpty(mCacheMode)) {
            connection.addRequestProperty(HTTP_CACHE_PROPERTY, mCacheMode);
        }

        if (mArgs != null) {

            for (int i = 0; i < mArgs.size(); ++i) {
                connection.setRequestProperty(mArgs.getKey(i), mArgs.getValue(i));
            }
        }

        if(POST.equals(mRequestMethod)) {
            handleConfig(connection);

            if (mBodyArgs != null && mBodyArgs.size() > 0) {
                connection.setDoOutput(true);
                final int loops = mBodyArgs.size() - 1;
                StringBuilder builder = new StringBuilder();

                try {

                    for (int i = 0; i < loops; ++i) {
                        builder.append(mBodyArgs.getKey(i)).append("=")
                                .append(URLEncoder.encode(mBodyArgs.getValue(i), "UTF-8"))
                                .append("&");
                    }

                    builder.append(mBodyArgs.getKey(loops)).append("=")
                            .append(URLEncoder.encode(mBodyArgs.getValue(loops), "UTF-8"));
                    OutputStream os = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(builder.toString());
                    writer.flush();
                    writer.close();
                    os.close();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }

        return connection;
    }

    private void handleConfig(HttpsURLConnection connection) {

        if (mRequestConfig != null) {
            Values vs = mRequestConfig.getBodyArguments(connection);

            if(vs != null) {

                if(mBodyArgs == null) {
                    mBodyArgs = new Values(vs.size());
                }

                for (Values.Value v : vs) {
                    mBodyArgs.add(v.key, v.value);
                }
            }
        }
    }

    public interface IPostRequestConfig {
        @Nullable
        Values getBodyArguments(HttpsURLConnection connection);
    }
}
