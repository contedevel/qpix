package com.contedevel.qpix.util;

/**
 * Created by ConteDevel on 23.08.2016.
 *
 * Classes, implementing this interface, have an empty constructor
 * and they must be initialized by method that specified in
 * the JavaDoc (e.g. 'init').
 */
public interface ILazyInstance {
}
