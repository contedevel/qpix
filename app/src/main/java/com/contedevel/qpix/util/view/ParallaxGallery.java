package com.contedevel.qpix.util.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.contedevel.qpix.R;
import com.contedevel.qpix.util.AccelerometerUtil;
import com.contedevel.qpix.util.ColorUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Created by ConteDevel on 24.08.2016.
 */
public class ParallaxGallery extends SurfaceView implements SurfaceHolder.Callback,
        SensorEventListener {

    private static final String TAG = ParallaxGallery.class.getSimpleName();

    private static final int INVALID_ID = -1;

    private Display mDisplay;
    private DrawThread mThread;
    private List<Bitmap> mPending = new ArrayList<>();

    private volatile float mFactor = 0;

    private float[] mAccelValues = new float[3];
    private int mMaxSize;
    private int mMinSize;
    private int mDivider;

    public ParallaxGallery(Context context) {
        super(context);
        init(context);
    }

    public ParallaxGallery(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ParallaxGallery(Context context, AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ParallaxGallery(Context context, AttributeSet attrs,
                           final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        getHolder().addCallback(this);
        WindowManager mWindowManager = (WindowManager) context
                .getSystemService(context.WINDOW_SERVICE);
        mDisplay = mWindowManager.getDefaultDisplay();

        Resources res = context.getResources();
        mMaxSize = res.getDimensionPixelSize(R.dimen.image_max_size);
        mMinSize = res.getDimensionPixelSize(R.dimen.image_min_size);
        mDivider = res.getDimensionPixelSize(R.dimen.image_divider_height);

        SensorManager sm = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        Sensor accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void add(Bitmap bmp) {

        if (mThread != null) {
            mThread.add(bmp);
        } else {
            mPending.add(bmp);
        }
    }

    public void addAll(Collection<Bitmap> bmps) {

        if (mThread != null) {
            mThread.addAll(bmps);
        } else {
            mPending.addAll(bmps);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        final int rotation = mDisplay.getRotation();
        AccelerometerUtil.canonicalToScreenOrientation(rotation, sensorEvent.values, mAccelValues);
        mFactor = Math.round(mAccelValues[0] * 10) / 10;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, final int accuracy) {
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        mThread = new DrawThread(getHolder());

        if(mPending.size() > 0) {
            mThread.addAll(mPending);
            mPending.clear();
        }

        mThread.setIsRunning(true);
        mThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, final int format,
                               final int w, final int h) {
        mThread.onResize(w, h);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        // Try to kill thread
        mThread.setIsRunning(false);

        while (retry) {

            try {
                mThread.join();
                retry = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static class Image {
        public int color = ColorUtils.generateColor(0.9f);
        public int imageId;
        public float x = 0, y = 0, z = 0;
        public float dx = 0;
        public int w = 1, h = 1;

        public Image() { }

        public Image(final int imageId, final int w, final int h,
                     final float x, final float y, final float z) {
            this.imageId = imageId;
            this.h = h;
            this.w = w;
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    private class DrawThread extends Thread {

        private SurfaceHolder mHolder;
        private Paint mPaint = new Paint();

        private boolean mIsRunning = false;

        private Random mRandom;
        private List<Image> mImages = new ArrayList<>();
        private List<Bitmap> mBitmaps = new ArrayList<>();

        private Rect mSrc = new Rect();
        private RectF mDst = new RectF();

        public DrawThread(SurfaceHolder holder) {
            mHolder = holder;
            mRandom = new Random(System.currentTimeMillis());
            mPaint.setStyle(Paint.Style.FILL);
        }

        public void setIsRunning(final boolean isRunning) {
            mIsRunning = isRunning;
        }

        @Override
        public void run() {
            Canvas canvas;

            while (mIsRunning) {
                canvas = null;

                try {
                    canvas = mHolder.lockCanvas(null);

                    synchronized (mHolder) {

                        if(canvas != null) {
                            canvas.drawColor(Color.WHITE);
                            final float factor = mFactor;

                            for (Image img : mImages) {
                                img.dx = -0.1f * factor * img.z;

                                if (INVALID_ID == img.imageId) {
                                    mPaint.setAlpha(0);
                                    mPaint.setColor(img.color);
                                    canvas.drawRect(img.x + img.dx, img.y,
                                            img.x + img.dx + img.w, img.y + img.h, mPaint);
                                } else {
                                    mPaint.setAlpha(100);
                                    Bitmap bmp = mBitmaps.get(img.imageId);
                                    mSrc.set(0, 0, bmp.getWidth(), bmp.getHeight());
                                    mDst.set(img.x + img.dx, img.y,
                                            img.x + img.dx + img.w, img.y + img.h);
                                    canvas.drawBitmap(bmp, mSrc, mDst, mPaint);
                                }
                            }
                        }
                    }
                } finally {

                    if (canvas != null) {
                        // Output a result to the screen
                        mHolder.unlockCanvasAndPost(canvas);
                    }
                }
            }
        }

        public synchronized void onResize(final int w, final int h) {
            final int parts = h / (mMaxSize + 2 * mDivider);
            final int offset = h % parts;
            final int step = h / parts;
            mImages.clear();

            for(int i = offset; i < h; i += step) {
                int size = mMinSize + mRandom.nextInt(mMaxSize - mMinSize);
                int left = mMinSize + mRandom.nextInt(mMaxSize - mMinSize);

                do {
                    int dy = mDivider + mRandom.nextInt(step - size - mDivider);
                    mImages.add(new Image(INVALID_ID, size, size, left, i + dy, step - size));
                    left += size + mMinSize + mRandom.nextInt(mMaxSize - mMinSize);
                    size = mMinSize + mRandom.nextInt(mMaxSize - mMinSize);
                } while ((left + size) <= w);
            }

            linkImages();
        }

        public synchronized void add(Bitmap bmp) {
            mBitmaps.add(bmp);
            final int position = mBitmaps.size() - 1;

            for (Image i : mImages) {

                if (INVALID_ID == i.imageId && mRandom.nextInt(2) > 0) {
                    i.imageId = position;
                }
            }

            invalidate();
        }

        public synchronized void addAll(Collection<Bitmap> bmps) {
            mBitmaps.addAll(bmps);
            linkImages();
            invalidate();
        }

        private void linkImages() {

            for (Image i : mImages) {

                if (INVALID_ID == i.imageId && mRandom.nextInt(mBitmaps.size() + 1) > 0) {
                    i.imageId = mRandom.nextInt(mBitmaps.size());
                }
            }
        }
    }
}
