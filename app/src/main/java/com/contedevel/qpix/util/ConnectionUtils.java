package com.contedevel.qpix.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ConteDevel on 22.08.2016.
 */
public abstract class ConnectionUtils {

    private ConnectionUtils() {}

    public static Map<String, String> getQueryMap(final String query) {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<>();

        for (String param : params) {
            String[] pair = param.split("=");
            map.put(pair[0], pair[1]);
        }

        return map;
    }

    public static String readToString(final InputStream is) {
        BufferedReader in = new BufferedReader(new InputStreamReader(is));
        String inputLine;
        StringBuilder response = new StringBuilder();

        try {

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }

            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response.toString();
    }
}
