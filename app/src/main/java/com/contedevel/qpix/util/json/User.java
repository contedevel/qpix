package com.contedevel.qpix.util.json;

import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by ConteDevel on 23.08.2016.
 */
public class User extends Json {

    private static final long serialVersionUID = -1776652352848118944L;

    private String mId;
    private String mUsername;
    private String mBio;
    private String mWebsite;
    private String mAvatarUrl;
    private String mFullName;

    @Nullable
    public String getAvatarUrl() {
        return mAvatarUrl;
    }

    public String getBio() {
        return mBio;
    }

    public String getFullName() {
        return mFullName;
    }

    public String getId() {
        return mId;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getWebsite() {
        return mWebsite;
    }

    @Override
    public void init(final String... args) {
        final String jsonStr = args[0];

        try {
            JSONObject json = new JSONObject(jsonStr);
            mId = json.getString("id");
            mUsername = json.getString("username");
            mBio = json.getString("bio");
            mWebsite = json.getString("website");
            mAvatarUrl = json.getString("profile_picture");

            if(json.has("full_name")) {
                mFullName = URLDecoder.decode(json.getString("full_name"), "UTF-8");
            } else {
                mFullName = URLDecoder.decode(json.getString("first_name")
                        + json.getString("last_name"), "UTF-8");
            }

            mIsOk = true;
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
            mIsOk = false;
        }
    }

    public static class UserPage extends Page<User> {

        private static final long serialVersionUID = -1353976651924577516L;

        @Override
        public User[] onArrayCreation(final int size) {
            return new User[size];
        }

        @Override
        public User onChildCreation() {
            return new User();
        }
    }
}
