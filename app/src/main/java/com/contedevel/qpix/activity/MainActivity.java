package com.contedevel.qpix.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.contedevel.qpix.R;
import com.contedevel.qpix.fragment.MainFragment;
import com.contedevel.qpix.util.Cache;
import com.contedevel.qpix.util.InstagramAPI;
import com.contedevel.qpix.util.NetStateReceiver;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ConteDevel on 22.08.2016.
 */

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavView;
    private AccountManager mAccountManager;

    private Fragment mFragment;
    private Account mAccount;

    private NetStateReceiver mNetStateReceiver = new NetStateReceiver() {

        @Override
        public void onNetStateChanged(final boolean hasConnection) {
            FragmentManager fm = getSupportFragmentManager();
            List<Fragment> fragments = fm.getFragments();

            if (fragments != null) {

                for (Fragment f : fragments) {

                    if (f instanceof OnNetStateChangedListener) {
                        ((OnNetStateChangedListener) f).onNetStateChanged(hasConnection);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAccountManager = AccountManager.get(this);
        mAccount = getAccount();

        if (mAccount == null) {
            Log.d(TAG, "addAccount");
            mAccountManager.addAccount(InstagramAPI.ACCOUNT_TYPE, InstagramAPI.KEY_ACCESS_TOKEN,
                    null, null, this, null, null);
        }

        setContentView(R.layout.activity_main);
        //Enable cache
        Cache.enable(this);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mNavView = (NavigationView) findViewById(R.id.navView);
        mNavView.setNavigationItemSelectedListener(this);

        if (mAccount != null) {
            updateHeader();

            FragmentManager fm = getSupportFragmentManager();
            mFragment = fm.findFragmentByTag("MainFragment");

            if (mFragment == null) {
                mFragment = new MainFragment();
                fm.beginTransaction()
                        .add(R.id.container, mFragment, "MainFragment")
                        .commit();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mNetStateReceiver, intentFilter);

        Account account = getAccount();

        if (account == null) {
            return;
        }

        if (mAccount == null || !mAccount.name.equals(account.name)) {
            mAccount = account;
            updateHeader();

            FragmentManager fm = getSupportFragmentManager();
            mFragment = new MainFragment();
            fm.beginTransaction()
                    .replace(R.id.container, mFragment, "MainFragment")
                    .commit();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mNetStateReceiver);
    }

    @Override
    protected void onStop() {
        Cache.disable();
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    private void updateHeader() {
        View header = mNavView.getHeaderView(0);
        TextView tvFullName = (TextView) header.findViewById(R.id.tvFullName);
        TextView tvUsername = (TextView) header.findViewById(R.id.tvUsername);
        CircleImageView civAvatar = (CircleImageView) header.findViewById(R.id.civAvatar);
        tvFullName.setText(mAccountManager.getUserData(mAccount, InstagramAPI.KEY_FULL_NAME));
        tvUsername.setText(mAccount.name);
        final int size = getResources().getDimensionPixelSize(R.dimen.avatar_size);
        Picasso.with(this)
                .load(mAccountManager.getUserData(mAccount, InstagramAPI.KEY_AVATAR_URL))
                .resize(size, size)
                .placeholder(R.mipmap.ic_launcher)
                .into(civAvatar);
    }

    @Nullable
    public Account getAccount() {
        Account[] accounts = mAccountManager.getAccountsByType(InstagramAPI.ACCOUNT_TYPE);

        if (accounts.length > 0) {
            return accounts[0];
        }

        return null;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one,
        //just so we can grab a window token from it.
        if (view == null) {
            view = new View(this);
        }

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
