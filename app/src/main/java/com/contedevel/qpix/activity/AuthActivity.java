package com.contedevel.qpix.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.contedevel.qpix.R;
import com.contedevel.qpix.util.InstagramAPI;
import com.contedevel.qpix.util.VectorDrawable;
import com.contedevel.qpix.util.json.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ConteDevel on 23.08.2016.
 */
public class AuthActivity extends AppCompatActivity
        implements LoaderManager.LoaderCallbacks<String> {

    private static final String TAG = AuthActivity.class.getSimpleName();

    public static final String KEY_AUTH_TOKEN_TYPE      = "auth_token_type";
    public static final String KEY_REQUIRED_FEATURES    = "required_features";

    private static final int ID_LOADER_TOKEN = 0;

    private static final String LOAD_URL = InstagramAPI.URL_DOMAIN + InstagramAPI.URL_AUTH + "?"
            + InstagramAPI.KEY_CLIENT_ID + "=" + InstagramAPI.CLIENT_ID + "&"
            + InstagramAPI.KEY_REDIRECT_URI + "=" + InstagramAPI.REDIRECT_URI + "&"
            + InstagramAPI.KEY_RESPONSE_TYPE + "=code&scope=public_content";

    private TextView mTVMsg;
    private WebView mWebView;

    private String mAccountName;
    private boolean mIsRunning = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null) {
            mIsRunning = savedInstanceState.getBoolean("is_running");
        }

        if(!mIsRunning) {
            Intent args = getIntent();

            if (!args.hasExtra(AccountManager.KEY_ACCOUNT_NAME)) {
                final String accountType = args.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

                if (TextUtils.isEmpty(accountType)
                        && !InstagramAPI.ACCOUNT_TYPE.equals(accountType)) {
                    Toast.makeText(this, R.string.error_invalid_account_type, Toast.LENGTH_LONG)
                            .show();
                    finish();
                }

                AccountManager am = AccountManager.get(this);
                Account[] accounts = am.getAccountsByType(accountType);

                if (accounts.length > 0) {
                    Toast.makeText(this, R.string.error_account_exists, Toast.LENGTH_LONG)
                            .show();
                    finish();
                }
            } else {
                mAccountName = args.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            }
        }

        setContentView(R.layout.activity_auth);
        //Set ActionBar
        initActionBar();
        //Set TextView for messages
        mTVMsg = (TextView) findViewById(R.id.tvMessage);
        //Set WebView
        mWebView = (WebView) findViewById(R.id.webView);

        if (savedInstanceState != null) {
            mTVMsg.setText(savedInstanceState.getString("msg"));
            mTVMsg.setTextColor(savedInstanceState.getInt("msg_color"));
            final int webVisibility = savedInstanceState.getInt("web_visibility");

            if(webVisibility == View.VISIBLE) {
                mWebView.setWebViewClient(new WebQpixClient());
                mWebView.restoreState(savedInstanceState);
            } else {
                mWebView.setVisibility(View.GONE);
                mTVMsg.setVisibility(View.VISIBLE);
            }

        } else {
            mWebView.setWebViewClient(new WebQpixClient());
            mWebView.loadUrl(LOAD_URL);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWebView.saveState(outState);
        outState.putBoolean("is_running", true);
        outState.putString("msg", mTVMsg.getText().toString());
        outState.putInt("msg_color", mTVMsg.getCurrentTextColor());
        outState.putInt("web_visibility", mWebView.getVisibility());
        mIsRunning = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (android.R.id.home == item.getItemId()) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            Drawable upIndicator = VectorDrawable.getDrawable(this, R.drawable.ic_close_white_24dp);
            actionBar.setHomeAsUpIndicator(upIndicator);
        }
    }

    public void onRequestResult(final String code) {
        Bundle args = new Bundle(1);
        args.putString(InstagramAPI.KEY_CODE, code);
        getSupportLoaderManager().initLoader(ID_LOADER_TOKEN, args, this).forceLoad();
    }

    @Override
    public Loader<String> onCreateLoader(final int id, Bundle args) {

        if(ID_LOADER_TOKEN == id && args.containsKey(InstagramAPI.KEY_CODE)) {
            return new TokenLoader(this, args.getString(InstagramAPI.KEY_CODE));
        }

        return null;
    }

    @Override
    public void onLoadFinished(Loader<String> loader, final String data) {

        if(TextUtils.isEmpty(data)) {
            mTVMsg.setText(R.string.error_auth_failed);
            mTVMsg.setTextColor(ContextCompat.getColor(this, R.color.red_54));
            return;
        }

        try {
            JSONObject json = new JSONObject(data);
            final String accessToken = json.getString(InstagramAPI.KEY_ACCESS_TOKEN);
            final String profileJsonStr = json.getString("user");
            User self = new User();
            self.init(profileJsonStr);

            if(self.isOk()) {
                AccountManager am = AccountManager.get(this);
                Account account = new Account(self.getUsername(), InstagramAPI.ACCOUNT_TYPE);

                if (!TextUtils.isEmpty(mAccountName)) {
                    Account oldAccount = new Account(mAccountName, InstagramAPI.ACCOUNT_TYPE);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        am.removeAccount(oldAccount, this, null, null);
                    } else {
                        am.removeAccount(oldAccount, null, null);
                    }
                }

                am.addAccountExplicitly(account, null, new Bundle());
                am.setAuthToken(account, InstagramAPI.KEY_ACCESS_TOKEN, accessToken);
                am.setUserData(account, InstagramAPI.KEY_ACCESS_TOKEN, accessToken);
                am.setUserData(account, InstagramAPI.KEY_AVATAR_URL, self.getAvatarUrl());
                am.setUserData(account, InstagramAPI.KEY_FULL_NAME, self.getFullName());
                am.setUserData(account, InstagramAPI.KEY_ID, self.getId());

                mTVMsg.setText(R.string.msg_auth_successful);
                mTVMsg.setTextColor(ContextCompat.getColor(this, R.color.green_dark_54));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            mTVMsg.setText(R.string.error_auth_failed);
            mTVMsg.setTextColor(ContextCompat.getColor(this, R.color.red_54));
        }
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {

    }

    public class WebQpixClient extends WebViewClient {

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            onUrlLoading(view, request.getUrl().toString());
            return super.shouldOverrideUrlLoading(view, request);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                onUrlLoading(view, url);
            }

            return super.shouldOverrideUrlLoading(view, url);
        }

        public void onUrlLoading(WebView webView, final String url) {

            if(url.startsWith(InstagramAPI.REDIRECT_URI)) {
                webView.stopLoading();
                final String prefix = InstagramAPI.KEY_CODE + "=";
                final int startIndex = url.indexOf(prefix);
                webView.setVisibility(View.GONE);
                mTVMsg.setVisibility(View.VISIBLE);

                if(startIndex < 0) {
                    mTVMsg.setText(R.string.error_auth_failed);
                    mTVMsg.setTextColor(ContextCompat
                            .getColor(AuthActivity.this, R.color.red_54));
                } else {
                    mTVMsg.setText(R.string.msg_loading_user_data);
                    mTVMsg.setTextColor(ContextCompat
                            .getColor(AuthActivity.this, R.color.black_54));
                    final String code = url.substring(startIndex + prefix.length());
                    onRequestResult(code);
                }
            }
        }
    }

    public static final class TokenLoader extends AsyncTaskLoader<String> {
        private final String mCode;

        public TokenLoader(Context context, final String code) {
            super(context);
            mCode = code;
        }

        @Override
        public String loadInBackground() {

            try {
                return InstagramAPI.requestToken(mCode);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
    }
}
